import json
import requests
import time

from .models import ConferenceVO

# python manage.py crontab run 3e42e2a3a251f27f78d3b4b43c4dd26f


def get_conferences():
    while True:
        url = "http://monolith:8000/api/conferences/"
        response = requests.get(url)
        content = json.loads(response.content)
        for conference in content["conferences"]:
            ConferenceVO.objects.update_or_create(
                import_href=conference["href"],
                defaults={"name": conference["name"]},
            )
        time.sleep(60)
