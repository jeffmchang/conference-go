from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": city + " " + state}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    # Use the Open Weather API
    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    # and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    # them in a dictionary
    # Return the dictionary

    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    content = json.loads(response.content)
    lat = content[0]["lat"]
    lon = content[0]["lon"]
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(weather_url)
    content = json.loads(response.content)

    try:
        return {
            "temp": content["main"]["temp"],
            "description": content["weather"][0]["description"],
        }
    except:
        return {
            "temp": None,
            "description": None,
        }
